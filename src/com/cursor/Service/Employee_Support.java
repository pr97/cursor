package com.cursor.Service;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class Employee_Support{
	
	public static boolean scoreUpdater(int empid,HttpServletRequest req,HttpServletResponse res) throws IOException
	{
		Employee_Support es=new Employee_Support();
		PrintWriter out=res.getWriter();
		String url="jdbc:mysql://localhost:3306/Rahul";
		String user="root";
		String pass="rahul";
		String query="SELECT DispatchDescription,DispatchId FROM Empassign where idEmpassign=?";
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection(url, user, pass);
			PreparedStatement pt=con.prepareStatement(query);
			pt.setInt(1, empid);
			pt.execute();
			ResultSet rs=pt.executeQuery();
			
			while (rs.next()) {
				
				out.println("\n"+rs.getString("DispatchDescription")+"-------------------- "+rs.getString("DispatchId")+"\n");
				
				es.viewScore(empid, req, res);
				
				
				
			}
			rs.close();
			pt.close();
			con.close();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return false;
		
	}
	
	public static void viewScore(int Empid,HttpServletRequest req,HttpServletResponse res) throws IOException
	{
		PrintWriter out=res.getWriter();
		String url="jdbc:mysql://localhost:3306/Rahul";
		String user="root";
		String pass="rahul";
		String query="select sum(EmpJobScore)as EmpJobScore from Empassign where idEmpassign=?";
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection(url, user, pass);
			PreparedStatement pt=con.prepareStatement(query);
			pt.setInt(1,Empid);
			pt.execute();
			ResultSet rs=pt.executeQuery();
			while (rs.next()) 
			{
				int score=rs.getInt("EmpJobScore");
				out.println("Your Score is :  "+score);
			}
			rs.close();
			pt.close();
			con.close();
			
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}

}
