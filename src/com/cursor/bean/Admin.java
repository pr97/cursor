package com.cursor.bean;

public class Admin
{
	
	String aid="123";
	String aname="abc";
	String eid;
	String ename;
	int ejobscore=0;
	Long dispatchid;
	String dispatch_details;
	public Admin() {
		// TODO Auto-generated constructor stub
		dispatchid=System.currentTimeMillis()*9;
	}
	public String getAid() {
		return aid;
	}
	public void setAid(String aid) {
		this.aid = aid;
	}
	public String getAname() {
		return aname;
	}
	public void setAname(String aname) {
		this.aname = aname;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public int getEjobscore() {
		return ejobscore;
	}
	public void setEjobscore(int ejobscore) {
		this.ejobscore = ejobscore;
	}
	public Long getDispatchid() {
		return dispatchid;
	}
	public void setDispatchid(String dispatchid) {
		this.dispatchid = Long.parseLong(dispatchid);
	}
	public String getDispatch_details() {
		return dispatch_details;
	}
	public void setDispatch_details(String dispatch_details) {
		this.dispatch_details = dispatch_details;
	}
	
	

}
