package com.cursor.roles;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cursor.bean.Admin;

/**
 * Servlet implementation class Adminop
 */
@WebServlet("/Adminop")
public class Adminop extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();
		
		if(request.getParameter("recruit")!=null)
		{
			Admin ad=new Admin();
			ad.setEid(request.getParameter("eid"));
			ad.setEname(request.getParameter("ename"));
			String url="jdbc:mysql://localhost:3306/Rahul";
			String user="root";
			String pass="rahul";
			String query="Insert into Empadd values(?,?)";
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con=DriverManager.getConnection(url, user, pass);
				PreparedStatement ps=con.prepareStatement(query);
				ps.setString(1, ad.getEid());
				ps.setString(2, ad.getEname());
				int row=ps.executeUpdate();
				out.println(row+" Employee Recruited");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		}
		if (request.getParameter("assign")!=null) {
			Admin ad=new Admin();
			ad.setEid(request.getParameter("eid"));
			ad.setDispatch_details(request.getParameter("dispatchdetails"));
			ad.setEjobscore(+1);
			String url="jdbc:mysql://localhost:3306/Rahul";
			String user="root";
			String pass="rahul";
			String query="SELECT * FROM Empadd";
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con=DriverManager.getConnection(url,user,pass);
				Statement st=con.createStatement();
				ResultSet rs=st.executeQuery(query);
				while(rs.next())
				{
					String eid=rs.getString("idEmpadd");
					if(ad.getEid().equals(eid))
					{
						String emp_assign_query="Insert into Empassign values(?,?,?,?)";
						PreparedStatement ps=con.prepareStatement(emp_assign_query);
						ps.setString(1, ad.getEid());
						ps.setString(2, ad.getDispatch_details());
						ps.setInt(3, ad.getEjobscore());
						ps.setShort(4,ad.getDispatchid().shortValue());
						int assign=ps.executeUpdate();
						out.println(assign+" delivery assigned to  "+ad.getEid());
						out.println("Your dispatch id is: "+ad.getDispatchid().shortValue());
						ps.close();
						
						
						
					}
				}
				st.close();
				con.close();
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
			
			
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
