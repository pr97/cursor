package com.cursor.roles;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.cursor.Service.Employee_Support;
/**
 * Servlet implementation class EmployeeOp
 */
@WebServlet("/EmployeeOp")
public class EmployeeOp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out=response.getWriter();
		if(request.getParameter("empJob")!=null)
		{
			String empid=request.getParameter("empid");
			int id2=Integer.parseInt(empid);
			String url="jdbc:mysql://localhost:3306/Rahul";
			String user="root";
			String pass="rahul";
			String basequery="SELECT * FROM Empadd;";
			try 
			{
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con=DriverManager.getConnection(url, user, pass);
				Statement st=con.createStatement();
				ResultSet rs=st.executeQuery(basequery);	
				while (rs.next()) 
				{
					String id=rs.getString("idEmpadd");
					if(id.equals(empid))
					{
						Employee_Support.scoreUpdater(id2, request, response);
					}
				}
				rs.close();
				st.close();
				con.close();
				
			} 
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		if(request.getParameter("jobscore")!=null)
		{
			String id=request.getParameter("empid");
			int id2=Integer.parseInt(id);
			Employee_Support.viewScore(id2, request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
